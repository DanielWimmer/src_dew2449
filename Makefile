
src: main.o roundup.o
	gcc -o src main.o roundup.o

main.o: main.c roundup.h
	gcc -c main.c

roundup.o: roundup.c roundup.h
	gcc -c roundup.c
