#include "roundup.h"

int roundup(int number_to_add, int Integers[])
{
   int i;
   int total = 0;
   for(i = 0; i < number_to_add; i++)
   {
      total += Integers[i];
   }

   return total;
}
